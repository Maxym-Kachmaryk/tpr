from prettytable import PrettyTable

def getFile():
    text = []
    with open("lab4.txt", "r") as file:
        lines = file.readlines()
        for i in lines:
            text.append(i.replace('\n', ''))
    for i in range(len(text)):
        text[i] = text[i].split(" ")
    return text

def evaluate(array):
    for i in array:
        for j in range(1, len(i)):
            i[j] = round(i[j] * i[0], 2)
    return array

def sum_of_column(array):
    return [sum(col) for col in zip(*array)]

array = getFile()
array = [[int(j) if '.' not in j else float(j) for j in i] for i in array]
array_of_variables = ['Arrival','Cost','Avg sea temperature','Scenery','Architecture']

table = PrettyTable(['№', 'Parameter', 'Weight', 'A Croatia', 'B Turkey', 'C Egypt', 'D OAE', 'E Portugal'])
for i in range(len(array_of_variables)):
    table.add_row([(i+1), array_of_variables[i], array[i][0], array[i][1], array[i][2], array[i][3], array[i][4], array[i][5]])
table.add_row(['Sum', '', 1, '', '', '', '', ''])

print(table)

print("\n Calculated table:")
array = evaluate(array)
total = [round(i, 3) for i in sum_of_column(array)]

table = PrettyTable(['№', 'Parameter', 'Weight', 'A Croatia', 'B Turkey', 'C Egypt', 'D OAE', 'E Portugal'])
for i in range(len(array_of_variables)):
    table.add_row([(i+1), array_of_variables[i], array[i][0], array[i][1], array[i][2], array[i][3], array[i][4], array[i][5]])
table.add_row(['Sum', '', total[0], total[1], total[2], total[3], total[4], total[5]])
print(table)

objects = ['A Croatia', 'B Turkey', 'C Egypt', 'D OAE', 'E Portugal']
total.pop(0)
print("\tChoice:", objects[total.index(max(total))], 'with mark -', max(total))
